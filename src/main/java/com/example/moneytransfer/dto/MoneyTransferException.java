package com.example.moneytransfer.dto;

import com.example.moneytransfer.utils.ErrorCodes;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

public class MoneyTransferException extends Exception {
    private int code;
    private int httpCode;

    public MoneyTransferException(String message, int code, int httpCode) {
        super(message);
        this.code = code;
        this.httpCode = httpCode;
    }

    public static MoneyTransferException notFoundError() {
        return notFoundError("Record not found with the provided id");
    }

    public static MoneyTransferException notFoundError(String message) {
        return notFoundError(message, ErrorCodes.NOT_FOUND);
    }

    public static MoneyTransferException notFoundError(String message, int code) {
        return new MoneyTransferException(message, code, SC_NOT_FOUND);
    }

    public static MoneyTransferException isNotValidId(String id) {
        return badRequest(String.format("'%s' is not a valid id ", id), ErrorCodes.INVALID_ID);
    }

    public static MoneyTransferException badRequest(String message) {
        return badRequest(message, ErrorCodes.GENERIC_BAD_REQUEST);
    }

    public static MoneyTransferException badRequest(String message, int code) {
        return new MoneyTransferException(message, code, SC_BAD_REQUEST);
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }
}
