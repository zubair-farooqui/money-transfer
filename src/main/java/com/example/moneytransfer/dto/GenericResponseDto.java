package com.example.moneytransfer.dto;

public class GenericResponseDto<T> {
    private T data;
    private MoneyTransferException error;

    public GenericResponseDto(MoneyTransferException error) {
        this.error = error;
    }

    public GenericResponseDto(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public MoneyTransferException getError() {
        return error;
    }

    public void setError(MoneyTransferException error) {
        this.error = error;
    }

    public boolean hasError() {
        return this.error != null;
    }

    public boolean isEmpty() {
        return this.data == null && !hasError();
    }
}
