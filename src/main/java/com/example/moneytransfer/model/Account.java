package com.example.moneytransfer.model;

public class Account {
    private long id;
    private String title;
    private long cents = 0;

    public Account(long id, String title, long cents) {
        this.id = id;
        this.cents = cents;
        this.title = title;
    }

    public Account(String title, long cents) {
        this.cents = cents;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCents() {
        return cents;
    }

    public void setCents(long cents) {
        this.cents = cents;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addCents(long cents) {
        this.cents += cents;
    }

    public void subtractCents(long cents) {
        this.cents -= cents;
    }
}
