package com.example.moneytransfer.service;


import com.example.moneytransfer.dto.MoneyTransferException;

public interface BaseService<T> {
    T get(long id) throws MoneyTransferException;
}
