package com.example.moneytransfer.service;

import com.example.moneytransfer.dao.AccountDaoImpl;
import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.model.Account;

public class AccountServiceImpl implements AccountService {

    private final AccountDaoImpl accountDaoImpl;

    public AccountServiceImpl() {
        accountDaoImpl = new AccountDaoImpl();
    }

    public AccountServiceImpl(AccountDaoImpl accountDaoImpl) {
        this.accountDaoImpl = accountDaoImpl;
    }

    @Override
    public Account get(long id) throws MoneyTransferException {
        return accountDaoImpl.get(id);
    }

    @Override
    public Account transfer(long sourceAccountId, long destinationAccountId, long cents) throws MoneyTransferException {
        Account sourceAccount = get(sourceAccountId);
        if (sourceAccount == null) {
            throw MoneyTransferException.notFoundError(String.format("Source account not found with id %d", sourceAccountId));
        }
        if (sourceAccount.getCents() < cents) {
            throw MoneyTransferException.badRequest("Source account doesnt have enough cents to transfer");
        }

        Account destinationAccount = get(destinationAccountId);
        if (destinationAccount == null) {
            throw MoneyTransferException.notFoundError(String.format("Destination account not found with id %d", destinationAccount));
        }

        sourceAccount.subtractCents(cents);
        destinationAccount.addCents(cents);
        if (!accountDaoImpl.transferCents(sourceAccount, destinationAccount, cents)) {
            throw MoneyTransferException.badRequest("Unable to transfer amount");
        }
        return sourceAccount;
    }

}
