package com.example.moneytransfer.service;

import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.model.Account;

public interface AccountService extends BaseService<Account> {
    Account transfer(long sourceAccountId, long destinationAccountId, long cents) throws MoneyTransferException;
}
