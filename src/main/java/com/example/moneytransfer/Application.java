package com.example.moneytransfer;

import com.example.moneytransfer.utils.H2JDBCUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static spark.Spark.*;

public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        port(8080);

        initExceptionHandler((e) -> logger.error("Uncaught error", e));

        H2JDBCUtils.refreshDb();

        Routing.init();
        awaitInitialization();
    }
}