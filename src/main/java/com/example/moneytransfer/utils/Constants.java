package com.example.moneytransfer.utils;

public class Constants {
    public static final String REQUEST_PARAM_ID = ":id";
    public static final String CONTENT_TYPE_JSON = "application/json";
}
