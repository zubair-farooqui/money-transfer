package com.example.moneytransfer.utils;

import com.example.moneytransfer.dao.AccountDaoImpl;
import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.model.Account;
import org.h2.tools.DeleteDbFiles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;

public class H2JDBCUtils {

    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_NAME = "money_transfer";
    private static final String DB_DIR = "./";
    private static final String DB_CONNECTION = "jdbc:h2:" + DB_DIR + "/" + DB_NAME;
    private static final String DB_USER = "";
    private static final String DB_PASSWORD = "";
    private static final Logger logger = LoggerFactory.getLogger(H2JDBCUtils.class);
    public static final Account TEST_ACCOUNT_1 = new Account("Account 1", 1000);

    public static Connection getConnection() {
        Connection connection = null;

        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        } catch (Exception e) {
            logger.error("Error getting connection", e);
        }
        return connection;
    }

    public static void deleteDb() {
        logger.info("Cleaning DB");
        DeleteDbFiles.execute(DB_DIR, DB_NAME, false);
        logger.info("DB Cleaned");
    }

    public static void refreshDb() {
        deleteDb();
        AccountDaoImpl accountDao = new AccountDaoImpl();
        try {
            accountDao.createTable();
            accountDao.insert(TEST_ACCOUNT_1);
            accountDao.insert(new Account("Account 2", 2000));
        } catch (MoneyTransferException e) {
            logger.error("Unable to insert account", e);
        }
    }

}
