package com.example.moneytransfer.utils;

import com.example.moneytransfer.dto.MoneyTransferException;

public class Utils {

    public static long parseLong(String param, String value) throws MoneyTransferException {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException ex) {
            throw MoneyTransferException.badRequest(String.format("'%s' is not a valid value for %s", value, param));
        }
    }
}
