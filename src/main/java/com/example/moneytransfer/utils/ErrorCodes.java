package com.example.moneytransfer.utils;

public class ErrorCodes {
    public static final int NOT_FOUND = 1;
    public static final int INVALID_ID = 2;
    public static final int GENERIC_BAD_REQUEST = 3;
}
