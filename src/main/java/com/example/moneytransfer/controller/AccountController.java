package com.example.moneytransfer.controller;

import com.example.moneytransfer.dto.GenericResponseDto;
import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.service.AccountService;
import com.example.moneytransfer.service.AccountServiceImpl;
import com.example.moneytransfer.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import spark.Request;
import spark.Response;
import spark.Route;

import static com.example.moneytransfer.utils.Constants.REQUEST_PARAM_ID;


public class AccountController {
    private static final String REQUEST_PARAM_DESTINATION_ID = ":destinationId";
    private static final String KEY_CENTS = "cents";

    private static final AccountService accountService = new AccountServiceImpl();
    private static final Gson gson = new Gson();

    public static Route transferCents = (Request request, Response response) -> {
        String idParam = request.params(REQUEST_PARAM_ID);
        String destinationIdParam = request.params(REQUEST_PARAM_DESTINATION_ID);
        JsonElement jsonElement;
        try {
            long id = Utils.parseLong(REQUEST_PARAM_ID, idParam);
            if (id < 1) {
                return new GenericResponseDto<>(MoneyTransferException.isNotValidId(idParam));
            }

            long destinationId = Utils.parseLong(REQUEST_PARAM_DESTINATION_ID, destinationIdParam);
            if (destinationId < 1) {
                return new GenericResponseDto<>(MoneyTransferException.isNotValidId(destinationIdParam));
            }

            JsonObject jsonObject = gson.fromJson(request.body(), JsonObject.class);
            jsonElement = jsonObject.get(KEY_CENTS);
            if (jsonElement == null) {
                return new GenericResponseDto<>(MoneyTransferException.badRequest("Money transfer cannot be possible without cents"));
            }

            long cents = Utils.parseLong(KEY_CENTS, jsonElement.getAsString());
            if (cents < 1) {
                return new GenericResponseDto<>(MoneyTransferException.badRequest("Minimum transfer amount is 1 cents"));
            }

            return new GenericResponseDto<>(accountService.transfer(id, destinationId, cents));
        } catch (MoneyTransferException ex) {
            return new GenericResponseDto<>(ex);
        }

    };

}
