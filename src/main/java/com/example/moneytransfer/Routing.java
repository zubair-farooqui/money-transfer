package com.example.moneytransfer;

import com.example.moneytransfer.controller.AccountController;
import com.example.moneytransfer.dto.GenericResponseDto;
import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.utils.JsonTransformer;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.example.moneytransfer.utils.Constants.CONTENT_TYPE_JSON;
import static spark.Spark.*;

public class Routing {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);
    private static final Gson gson = new Gson();

    public static void init() {
        get("/", (req, res) -> "{\"message\": \"Money Transfer Application\" }");


        path("/api", () -> {
            before("/*", (req, res) -> logger.info(new StringBuilder("Received api call ")
                    .append(req.requestMethod())
                    .append(" ")
                    .append(req.uri())
                    .toString()));

            path("/accounts", () -> {
                put("/:id/transfer/:destinationId", AccountController.transferCents, new JsonTransformer());
            });
        });

        after((req, res) -> {
            if (res.body() != null) {
                res.type(CONTENT_TYPE_JSON);
                GenericResponseDto responseDto = gson.fromJson(res.body(), GenericResponseDto.class);

                if (responseDto.isEmpty()) {
                    responseDto = new GenericResponseDto<>(MoneyTransferException.notFoundError());
                    res.body(gson.toJson(responseDto));
                }

                if (responseDto.hasError()) {
                    res.status(responseDto.getError().getHttpCode());
                }
            }
        });
    }
}
