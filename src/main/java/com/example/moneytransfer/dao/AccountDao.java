package com.example.moneytransfer.dao;

import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.model.Account;

public interface AccountDao extends BaseDao<Account> {
    boolean transferCents(Account source, Account destination, long transferAmount) throws MoneyTransferException;
}
