package com.example.moneytransfer.dao;

import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.utils.H2JDBCUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public interface BaseDao<T> {

    Logger logger = LoggerFactory.getLogger(BaseDao.class);

    String getCreateTableQuery();

    T get(long id) throws MoneyTransferException;

    long insert(T object) throws MoneyTransferException;

    default void createTable() throws MoneyTransferException {
        String query = getCreateTableQuery();
        try (Connection connection = H2JDBCUtils.getConnection();
             Statement statement = connection.createStatement();) {
            statement.execute(query);
        } catch (SQLException e) {
            logger.error("Error creating table", e);
            throw MoneyTransferException.badRequest(e.getMessage());
        }
    }


}
