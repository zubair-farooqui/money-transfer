package com.example.moneytransfer.dao;

import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.model.Account;
import com.example.moneytransfer.utils.H2JDBCUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class AccountDaoImpl implements AccountDao {

    private static final String SQL_CREATE_TABLE = "CREATE TABLE ACCOUNTS(ID IDENTITY PRIMARY KEY, TITLE VARCHAR(64), CENTS BIGINT)";
    private static final String SQL_SELECT_ONE = "SELECT * FROM ACCOUNTS WHERE ID = ?";
    private static final String SQL_INSERT = "INSERT INTO ACCOUNTS(TITLE, CENTS) VALUES(? , ?)";
    private static final String SQL_UPDATE_CENTS = "UPDATE ACCOUNTS SET CENTS = ? WHERE ID = ?";
    private static final Logger logger = LoggerFactory.getLogger(AccountDaoImpl.class);


    @Override
    public String getCreateTableQuery() {
        return SQL_CREATE_TABLE;
    }

    @Override
    public Account get(long id) throws MoneyTransferException {
        Account account = null;
        ResultSet resultSet;
        try (Connection connection = H2JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ONE);) {

            preparedStatement.setLong(1, id);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                account = new Account(resultSet.getLong(1), resultSet.getString(2), resultSet.getLong(3));
            }
        } catch (SQLException e) {
            logger.error("Error fetching account", e);
            throw MoneyTransferException.badRequest(e.getMessage());
        }
        return account;
    }

    @Override
    public long insert(Account object) throws MoneyTransferException {
        if (object == null) {
            throw MoneyTransferException.badRequest("Cannot insert null account");
        }
        long id = -1;
        try (Connection connection = H2JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {

            preparedStatement.setString(1, object.getTitle());
            preparedStatement.setLong(2, object.getCents());

            preparedStatement.execute();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
        } catch (SQLException e) {
            logger.error("Error inserting account", e);
            throw MoneyTransferException.badRequest(e.getMessage());
        }
        return id;
    }

    @Override
    public boolean transferCents(Account source, Account destination, long transferAmount) throws MoneyTransferException {
        if (source == null || destination == null) {
            throw MoneyTransferException.badRequest("Source or destination account cannot be null");
        }

        ResultSet resultSet;
        boolean updated = true;
        try (Connection connection = H2JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ONE);
             PreparedStatement preparedStatementSrc = connection.prepareStatement(SQL_UPDATE_CENTS, Statement.RETURN_GENERATED_KEYS);
             PreparedStatement preparedStatementDest = connection.prepareStatement(SQL_UPDATE_CENTS, Statement.RETURN_GENERATED_KEYS);) {
            // FIXME: Creating locks based on the string id is the quick solution. Proper solution will be creating own Mutex Lock using Collections.synchronizedMap or ConcurrentHashMap
            synchronized (String.valueOf(destination.getId()).intern()) {
                synchronized (String.valueOf(source.getId()).intern()) {
                    connection.setAutoCommit(false);

                    preparedStatement.setLong(1, source.getId());
                    resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        source = new Account(resultSet.getLong(1), resultSet.getString(2), resultSet.getLong(3));
                    }
                    if (source.getCents() < transferAmount) {
                        throw MoneyTransferException.badRequest("Source account doesnt have enough cents to transfer");
                    }

                    preparedStatement.setLong(1, destination.getId());
                    resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        destination = new Account(resultSet.getLong(1), resultSet.getString(2), resultSet.getLong(3));
                    }

                    source.subtractCents(transferAmount);
                    destination.addCents(transferAmount);

                    preparedStatementSrc.setLong(1, source.getCents());
                    preparedStatementSrc.setLong(2, source.getId());

                    preparedStatementDest.setLong(1, destination.getCents());
                    preparedStatementDest.setLong(2, destination.getId());

                    updated &= preparedStatementSrc.executeUpdate() > 0;
                    updated &= preparedStatementDest.executeUpdate() > 0;
                    if (updated) {
                        connection.commit();
                    } else {
                        connection.rollback();
                    }
                    connection.setAutoCommit(true);
                }
            }
        } catch (SQLException e) {
            logger.error("Error transferring money", e);
            throw MoneyTransferException.badRequest(e.getMessage());
        }
        return updated;
    }

}
