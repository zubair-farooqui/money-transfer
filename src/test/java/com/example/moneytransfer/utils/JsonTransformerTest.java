package com.example.moneytransfer.utils;

import com.example.moneytransfer.model.Account;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JsonTransformerTest {

    JsonTransformer jsonTransformer = new JsonTransformer();

    @Test
    public void testRender() {
        Account account = new Account("test", 1);
        assertEquals((jsonTransformer.render(account)), "{\"id\":0,\"title\":\"test\",\"cents\":1}");
    }

    @Test
    public void testRenderNullObjrct() {
        assertEquals((jsonTransformer.render(null)), null);
    }
}
