package com.example.moneytransfer.utils;

import com.example.moneytransfer.dao.AccountDao;
import com.example.moneytransfer.dao.AccountDaoImpl;
import com.example.moneytransfer.dto.MoneyTransferException;
import org.h2.jdbc.JdbcConnection;
import org.junit.Test;

import java.sql.Connection;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class H2JDBCUtilsTest {


    @Test
    public void testGetConnection() {
        Connection connection = H2JDBCUtils.getConnection();
        assertNotNull(connection);
        assertTrue(connection instanceof JdbcConnection);
    }

    @Test
    public void testDeleteDb() {
        H2JDBCUtils.deleteDb();

    }

    @Test
    public void testRefreshDb() throws MoneyTransferException {
        H2JDBCUtils.refreshDb();
        AccountDao accountDao = new AccountDaoImpl();
        assertNotNull(accountDao.get(1));
        ;
        assertNotNull(accountDao.get(2));
        ;
    }


}
