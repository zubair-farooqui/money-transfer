package com.example.moneytransfer.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class TestUtils {

    public static Object getFinalStaticFieldValue(Object object, String fieldName)
            throws ReflectiveOperationException {
        Field field = object.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        Field modifiers = Field.class.getDeclaredField("modifiers");
        modifiers.setAccessible(true);
        modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        return field.get(object);
    }
}
