package com.example.moneytransfer.utils;

import com.example.moneytransfer.dto.MoneyTransferException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testParseLongValid() throws MoneyTransferException {
        assertEquals(Utils.parseLong("test", "1"), 1);
    }

    @Test(expected = MoneyTransferException.class)
    public void testParseLongInvalid() throws MoneyTransferException {
        assertEquals(Utils.parseLong("test", "1asd"), 1);
    }
}
