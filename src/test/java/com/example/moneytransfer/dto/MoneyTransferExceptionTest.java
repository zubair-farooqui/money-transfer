package com.example.moneytransfer.dto;

import org.junit.Before;
import org.junit.Test;

import static com.example.moneytransfer.utils.ErrorCodes.*;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MoneyTransferExceptionTest {

    private MoneyTransferException moneyTransferException;
    private int TEST_HTTP_CODE = SC_BAD_REQUEST;
    private String TEST_MESSAGE = "test";
    private int TEST_CODE = 1;

    @Before
    public void setup() {
        moneyTransferException = new MoneyTransferException(TEST_MESSAGE, TEST_CODE, TEST_HTTP_CODE);
    }

    @Test
    public void testExceptionInheritance() {
        assertTrue(moneyTransferException instanceof Exception);
    }

    @Test
    public void testConstructor() {
        assertEquals(moneyTransferException.getMessage(), TEST_MESSAGE);
        assertEquals(moneyTransferException.getHttpCode(), TEST_HTTP_CODE);
        assertEquals(moneyTransferException.getCode(), TEST_CODE);
    }

    @Test
    public void testGetNotFoundError() {
        moneyTransferException = MoneyTransferException.notFoundError();
        assertEquals(moneyTransferException.getMessage(), "Record not found with the provided id");
        assertEquals(moneyTransferException.getCode(), NOT_FOUND);
        assertEquals(moneyTransferException.getHttpCode(), SC_NOT_FOUND);
    }

    @Test
    public void testGetNotFoundErrorMessage() {
        moneyTransferException = MoneyTransferException.notFoundError(TEST_MESSAGE);
        assertEquals(moneyTransferException.getMessage(), TEST_MESSAGE);
        assertEquals(moneyTransferException.getCode(), NOT_FOUND);
        assertEquals(moneyTransferException.getHttpCode(), SC_NOT_FOUND);
    }

    @Test
    public void testGetNotFoundErrorMessageAndCode() {
        moneyTransferException = MoneyTransferException.notFoundError(TEST_MESSAGE, TEST_CODE);
        assertEquals(moneyTransferException.getMessage(), TEST_MESSAGE);
        assertEquals(moneyTransferException.getCode(), TEST_CODE);
        assertEquals(moneyTransferException.getHttpCode(), SC_NOT_FOUND);
    }

    @Test
    public void testIsNotValidId() {
        moneyTransferException = MoneyTransferException.isNotValidId(TEST_MESSAGE);
        assertEquals(moneyTransferException.getMessage(), String.format("'%s' is not a valid id ", TEST_MESSAGE));
        assertEquals(moneyTransferException.getCode(), INVALID_ID);
        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
    }

    @Test
    public void testBadRequestMessage() {
        moneyTransferException = MoneyTransferException.badRequest(TEST_MESSAGE);
        assertEquals(moneyTransferException.getMessage(), TEST_MESSAGE);
        assertEquals(moneyTransferException.getCode(), GENERIC_BAD_REQUEST);
        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
    }

    @Test
    public void testBadRequestMessageAndCode() {
        moneyTransferException = MoneyTransferException.badRequest(TEST_MESSAGE, TEST_CODE);
        assertEquals(moneyTransferException.getMessage(), TEST_MESSAGE);
        assertEquals(moneyTransferException.getCode(), TEST_CODE);
        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
    }

    @Test
    public void testGetterAndSetterCode() {
        moneyTransferException.setCode(123);
        assertEquals(moneyTransferException.getCode(), 123);
    }

    @Test
    public void testGetterAndSetterHttpCode() {
        moneyTransferException.setHttpCode(123);
        assertEquals(moneyTransferException.getHttpCode(), 123);
    }
}
