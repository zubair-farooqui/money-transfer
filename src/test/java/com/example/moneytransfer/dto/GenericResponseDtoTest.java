package com.example.moneytransfer.dto;

import com.example.moneytransfer.model.Account;
import com.example.moneytransfer.utils.H2JDBCUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GenericResponseDtoTest {
    private GenericResponseDto genericResponseDataDto;
    private GenericResponseDto genericResponseErrorDto;

    @Before
    public void setup() {
        genericResponseDataDto = new GenericResponseDto(H2JDBCUtils.TEST_ACCOUNT_1);
        genericResponseErrorDto = new GenericResponseDto(MoneyTransferException.notFoundError());
    }

    @Test
    public void TestDataConstructor() {
        assertEquals(genericResponseDataDto.getData(), H2JDBCUtils.TEST_ACCOUNT_1);
        assertTrue(genericResponseDataDto.getData() instanceof Account);
        assertNull(genericResponseDataDto.getError());
        assertEquals(((Account) genericResponseDataDto.getData()).getId(), H2JDBCUtils.TEST_ACCOUNT_1.getId());
        assertEquals(((Account) genericResponseDataDto.getData()).getTitle(), H2JDBCUtils.TEST_ACCOUNT_1.getTitle());
        assertEquals(((Account) genericResponseDataDto.getData()).getCents(), H2JDBCUtils.TEST_ACCOUNT_1.getCents());
    }

    @Test
    public void TestErrorConstructor() {
        assertNull(genericResponseErrorDto.getData());
        assertTrue(genericResponseErrorDto.getError() instanceof MoneyTransferException);
    }

    @Test
    public void TestGetterAndSetterData() {
        Account account = new Account("test", 12);
        genericResponseDataDto.setData(account);
        assertEquals(genericResponseDataDto.getData(), account);
    }

    @Test
    public void TestGetterAndError() {
        MoneyTransferException moneyTransferException = MoneyTransferException.badRequest("TEST");
        genericResponseErrorDto.setError(moneyTransferException);
        assertEquals(genericResponseErrorDto.getError(), moneyTransferException);
    }

    @Test
    public void TestHasError() {
        assertFalse(genericResponseDataDto.hasError());
        assertTrue(genericResponseErrorDto.hasError());
    }

    @Test
    public void TestIsEmpty() {
        assertFalse(genericResponseDataDto.isEmpty());
        assertFalse(genericResponseErrorDto.isEmpty());
        assertTrue(new GenericResponseDto(null).isEmpty());
    }

}
