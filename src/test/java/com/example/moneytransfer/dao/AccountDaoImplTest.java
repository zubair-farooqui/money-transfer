package com.example.moneytransfer.dao;

import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.model.Account;
import com.example.moneytransfer.utils.H2JDBCUtils;
import org.junit.Before;
import org.junit.Test;

import static com.example.moneytransfer.utils.TestUtils.getFinalStaticFieldValue;
import static org.junit.Assert.*;

public class AccountDaoImplTest {

    AccountDao accountDao;

    @Before
    public void setup() {
        H2JDBCUtils.refreshDb();
        accountDao = new AccountDaoImpl();
    }

    @Test
    public void testGetCreateTableQuery() throws Exception {
        assertEquals(accountDao.getCreateTableQuery(), getFinalStaticFieldValue(accountDao, "SQL_CREATE_TABLE"));
    }

    @Test
    public void testGetValidId() throws MoneyTransferException {
        Account account = accountDao.get(1);
        assertNotNull(account);
        assertNotNull(account.getCents());
        assertNotNull(account.getTitle());
        assertEquals(account.getId(), 1);
        assertEquals(account.getTitle(), H2JDBCUtils.TEST_ACCOUNT_1.getTitle());
        assertEquals(account.getCents(), H2JDBCUtils.TEST_ACCOUNT_1.getCents());
    }

    @Test
    public void testGetInvalidValidId() throws MoneyTransferException {
        Account account = accountDao.get(3);
        assertNull(account);
    }

    @Test(expected = MoneyTransferException.class)
    public void testGetSQLException() throws MoneyTransferException {
        H2JDBCUtils.deleteDb();
        Account account = accountDao.get(3);
        assertNull(account);
    }

    @Test
    public void testInsertValidAccount() throws MoneyTransferException {
        Account testAccount = new Account("test", 2);
        long insertedAccountId = accountDao.insert(testAccount);
        Account insertedAccount = accountDao.get(insertedAccountId);
        assertNotNull(insertedAccount);
        assertEquals(insertedAccount.getId(), insertedAccountId);
        assertEquals(insertedAccount.getTitle(), testAccount.getTitle());
        assertEquals(insertedAccount.getCents(), testAccount.getCents());
    }

    @Test(expected = MoneyTransferException.class)
    public void testInsertNullAccount() throws MoneyTransferException {
        accountDao.insert(null);
    }

    @Test(expected = MoneyTransferException.class)
    public void testInsertSQLException() throws MoneyTransferException {
        H2JDBCUtils.deleteDb();
        accountDao.insert(H2JDBCUtils.TEST_ACCOUNT_1);
    }

    @Test
    public void testTransferCents() throws MoneyTransferException {
        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);
        account1.subtractCents(10);
        account2.addCents(10);
        assertTrue(accountDao.transferCents(account1, account2, 10));
        assertEquals(account1.getCents(), accountDao.get(1).getCents());
        assertEquals(account2.getCents(), accountDao.get(2).getCents());
    }

    @Test(expected = MoneyTransferException.class)
    public void testTransferCentsInsufficient() throws MoneyTransferException {
        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);
        accountDao.transferCents(account1, account2, 10000);
    }

    @Test(expected = MoneyTransferException.class)
    public void testTransferCentsNullSource() throws MoneyTransferException {
        Account account = accountDao.get(2);
        accountDao.transferCents(null, account, 10);
    }

    @Test(expected = MoneyTransferException.class)
    public void testTransferCentsNullDestination() throws MoneyTransferException {
        Account account = accountDao.get(2);
        accountDao.transferCents(account, null, 10);
    }

    @Test
    public void testTransferCentsInvalidSourceId() throws MoneyTransferException {
        Account account1 = accountDao.get(1);
        account1.setId(-1);
        Account account2 = accountDao.get(2);
        account1.subtractCents(10);
        account2.addCents(10);
        assertFalse(accountDao.transferCents(account1, account2, 10));
        assertNotEquals(account1.getCents(), accountDao.get(1).getCents());
        assertNotEquals(account2.getCents(), accountDao.get(2).getCents());
    }

    @Test
    public void testTransferCentsInvalidDestId() throws MoneyTransferException {
        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);
        account2.setId(-1);
        account1.subtractCents(10);
        account2.addCents(10);
        assertFalse(accountDao.transferCents(account1, account2, 10));
        assertNotEquals(account1.getCents(), accountDao.get(1).getCents());
        assertNotEquals(account2.getCents(), accountDao.get(2).getCents());
    }

    @Test(expected = MoneyTransferException.class)
    public void testTransferCentsSQLException() throws MoneyTransferException {
        H2JDBCUtils.deleteDb();
        accountDao.transferCents(H2JDBCUtils.TEST_ACCOUNT_1, H2JDBCUtils.TEST_ACCOUNT_1, 10);
    }

    @Test
    public void testCreateTable() throws MoneyTransferException {
        H2JDBCUtils.deleteDb();
        accountDao.createTable();
    }

    @Test(expected = MoneyTransferException.class)
    public void testCreateTableSQLException() throws MoneyTransferException {
        accountDao.createTable();
    }


}
