package com.example.moneytransfer.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccountTest {

    private static final String TITLE = "test";
    private static final long ID = 1;
    private static final long CENTS = 100;
    private Account account;

    @Before
    public void createTestAccount() {
        account = new Account(ID, TITLE, CENTS);
    }

    @Test
    public void testTwoArgumentConstructor() {
        account = new Account(TITLE, CENTS);
        assertEquals(account.getTitle(), TITLE);
        assertEquals(account.getCents(), CENTS);
        assertEquals(account.getId(), 0);
    }

    @Test
    public void testThreeArgumentConstructor() {
        account = new Account(ID, TITLE, CENTS);
        assertEquals(account.getId(), ID);
        assertEquals(account.getTitle(), TITLE);
        assertEquals(account.getCents(), CENTS);
    }

    @Test
    public void testGetterSetterId() {
        account.setId(2);
        assertEquals(account.getId(), 2);
    }

    @Test
    public void testGetterSetterTitle() {
        String newTitle = "new title";
        account.setTitle(newTitle);
        assertEquals(account.getTitle(), newTitle);
    }

    @Test
    public void testGetterSetterCents() {
        account.setCents(2);
        assertEquals(account.getCents(), 2);
    }

    @Test
    public void testAddCents() {
        account.addCents(2);
        assertEquals(account.getCents(), 2 + CENTS);
    }

    @Test
    public void testSubtractCents() {
        account.subtractCents(2);
        assertEquals(account.getCents(), CENTS - 2);
    }

}
