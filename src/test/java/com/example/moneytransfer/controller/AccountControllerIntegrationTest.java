package com.example.moneytransfer.controller;

import com.example.moneytransfer.Application;
import com.example.moneytransfer.Routing;
import com.example.moneytransfer.dao.AccountDao;
import com.example.moneytransfer.dao.AccountDaoImpl;
import com.example.moneytransfer.dto.GenericResponseDto;
import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.model.Account;
import com.example.moneytransfer.utils.Constants;
import com.example.moneytransfer.utils.ErrorCodes;
import com.example.moneytransfer.utils.H2JDBCUtils;
import com.example.moneytransfer.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static com.example.moneytransfer.utils.ErrorCodes.*;
import static javax.servlet.http.HttpServletResponse.*;
import static org.junit.Assert.*;
import static spark.Spark.stop;

public class AccountControllerIntegrationTest {
    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    private final AccountDao accountDao = new AccountDaoImpl();
    private final Gson gson = new Gson();

    @BeforeClass
    public static void setup() {
        String testArgs[] = new String[0];
        Application.main(testArgs);
    }

    @AfterClass
    public static void cleanUp() {
        stop();
    }

    @Before
    public void dbRefresh() {
        H2JDBCUtils.refreshDb();
    }

    @Test
    public void testTransferCents() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"500\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1/transfer/2"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_OK);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();

        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertFalse(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        Account returnedAccount = responseDto.getData();

        assertEquals(returnedAccount.getId(), 1);
        assertEquals(returnedAccount.getCents(), 500);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 500);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2500);
    }


    @Test
    public void testTransferCentsInvalidSourceAccountId() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"500\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/0/transfer/2"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_BAD_REQUEST);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
        assertEquals(moneyTransferException.getCode(), INVALID_ID);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }


    @Test
    public void testTransferCentsInvalidDestinationAccountId() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"500\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1/transfer/0"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_BAD_REQUEST);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
        assertEquals(moneyTransferException.getCode(), INVALID_ID);

        Account account1 = accountDao.get(1);
        H2JDBCUtils.refreshDb();
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }

    @Test
    public void testTransferCentsMissingCents() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1/transfer/2"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_BAD_REQUEST);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
        assertEquals(moneyTransferException.getCode(), GENERIC_BAD_REQUEST);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }

    @Test
    public void testTransferCentsNegativeCents() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"-1\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1/transfer/2"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_BAD_REQUEST);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
        assertEquals(moneyTransferException.getCode(), GENERIC_BAD_REQUEST);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }

    @Test
    public void testTransferStringSourceId() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"1\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1asd/transfer/2"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_BAD_REQUEST);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
        assertEquals(moneyTransferException.getCode(), GENERIC_BAD_REQUEST);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }

    @Test
    public void testTransferStringDestinationId() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"1\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1/transfer/2asd"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_BAD_REQUEST);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
        assertEquals(moneyTransferException.getCode(), GENERIC_BAD_REQUEST);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }

    @Test
    public void testTransferStringCents() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"asd1\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1/transfer/2"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_BAD_REQUEST);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
        assertEquals(moneyTransferException.getCode(), GENERIC_BAD_REQUEST);

        H2JDBCUtils.refreshDb();
        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }

    @Test
    public void testTransferMissingSourceId() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"111\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/3/transfer/2"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_NOT_FOUND);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_NOT_FOUND);
        assertEquals(moneyTransferException.getCode(), NOT_FOUND);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }


    @Test
    public void testTransferMissingDestinationId() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"111\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1/transfer/3"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_NOT_FOUND);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_NOT_FOUND);
        assertEquals(moneyTransferException.getCode(), NOT_FOUND);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }

    @Test
    public void testTransferInsufficientCents() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString("{\"cents\": \"1111\"}"))
                .uri(URI.create("http://localhost:8080/api/accounts/1/transfer/2"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_BAD_REQUEST);

        Type genericResponseAccountType = new TypeToken<GenericResponseDto<Account>>() {
        }.getType();
        GenericResponseDto<Account> responseDto = gson.fromJson(response.body(), genericResponseAccountType);

        assertTrue(responseDto.hasError());
        assertFalse(responseDto.isEmpty());

        MoneyTransferException moneyTransferException = responseDto.getError();

        assertEquals(moneyTransferException.getHttpCode(), SC_BAD_REQUEST);
        assertEquals(moneyTransferException.getCode(), GENERIC_BAD_REQUEST);

        Account account1 = accountDao.get(1);
        Account account2 = accountDao.get(2);

        assertEquals(account1.getId(), 1);
        assertEquals(account1.getCents(), 1000);
        assertEquals(account2.getId(), 2);
        assertEquals(account2.getCents(), 2000);
    }

    @Test
    public void testRoot() throws IOException, InterruptedException, MoneyTransferException {

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("http://localhost:8080/"))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), SC_NOT_FOUND);

    }

    @Test
    public void testConstructors() {
        AccountController accountController = new AccountController();
        Application application = new Application();
        H2JDBCUtils h2JDBCUtils = new H2JDBCUtils();
        Utils utils = new Utils();
        ErrorCodes errorCodes = new ErrorCodes();
        Constants constants = new Constants();
        Routing routing = new Routing();
    }
}
