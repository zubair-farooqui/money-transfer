package com.example.moneytransfer.service;

import com.example.moneytransfer.dao.AccountDaoImpl;
import com.example.moneytransfer.dto.MoneyTransferException;
import com.example.moneytransfer.model.Account;
import com.example.moneytransfer.utils.H2JDBCUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

    @Mock
    private AccountDaoImpl accountDaoImpl;

    @InjectMocks
    private AccountServiceImpl accountService;


    @Test
    public void testGetValidId() throws MoneyTransferException {
        when(accountDaoImpl.get(3L)).thenReturn(H2JDBCUtils.TEST_ACCOUNT_1);
        assertEquals(accountService.get(3L), H2JDBCUtils.TEST_ACCOUNT_1);
        Mockito.verify(accountDaoImpl).get(3L);
    }

    @Test
    public void testGetInvValidId() throws MoneyTransferException {
        assertNull(accountService.get(3L));
    }

    @Test
    public void testValidTransfer() throws MoneyTransferException {
        Account account1 = H2JDBCUtils.TEST_ACCOUNT_1;
        Account account2 = new Account("Test", 2000);
        account1.setCents(1000);

        when(accountDaoImpl.get(1L)).thenReturn(account1);
        when(accountDaoImpl.get(2L)).thenReturn(account2);
        when(accountDaoImpl.transferCents(any(Account.class), any(Account.class), anyLong())).thenReturn(true);

        Account returnedSourceAccount = accountService.transfer(1, 2, 100);

        assertEquals(returnedSourceAccount, account1);
        assertEquals(returnedSourceAccount.getCents(), 900);
        assertEquals(account2.getCents(), 2100);
    }

    @Test(expected = MoneyTransferException.class)
    public void testTransferInvalidSourceAccount() throws MoneyTransferException {
        accountService.transfer(1, 2, 100);
    }

    @Test(expected = MoneyTransferException.class)
    public void testTransferInvalidDestinationAccount() throws MoneyTransferException {
        Account account1 = H2JDBCUtils.TEST_ACCOUNT_1;
        when(accountDaoImpl.get(1L)).thenReturn(account1);
        accountService.transfer(1, 3, 100);
    }

    @Test(expected = MoneyTransferException.class)
    public void testTransferInsufficientCents() throws MoneyTransferException {
        Account account1 = H2JDBCUtils.TEST_ACCOUNT_1;
        account1.setCents(1);
        when(accountDaoImpl.get(1L)).thenReturn(account1);
        accountService.transfer(1, 2, 10000);
    }

    @Test(expected = MoneyTransferException.class)
    public void testInValidTransfer() throws MoneyTransferException {
        Account account1 = H2JDBCUtils.TEST_ACCOUNT_1;
        Account account2 = new Account("Test", 2000);
        account1.setCents(1000);

        when(accountDaoImpl.get(1L)).thenReturn(account1);
        when(accountDaoImpl.get(2L)).thenReturn(account2);
        when(accountDaoImpl.transferCents(any(Account.class), any(Account.class), anyLong())).thenReturn(false);

        accountService.transfer(1, 2, 100);

    }

}
