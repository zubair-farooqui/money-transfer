# Money Transfer Backend Web Application

A RESTful API (including data model and the backing implementation) for transferring money between accounts.

**Stack**
1) Java Spark Framework (http://sparkjava.com/) for web application  
2) H2 Database
3) Google GSON/JSON library used for json conversion
4) Junit & Mockito for unit/integration testing
5) slf4j-simple for logging (logfile along with its rotation policy are not configured for this version)


**Running Application:** 
Run `mvn clean package` command to build application jar file. Jar file will be created in target directory with name _moneytransfer-0.0.1-SNAPSHOT-jar-with-dependencies.jar_

Application will run on _port_ 8080 which can be configured in Application.java. When application starts, it creates following two Accounts
 1) title "Account 1" with balance 1000 cents 
 2) title "Account 2" with balance 2000 cents 

In this minimal application version, we have only **one** API which is for transferring money from one account to another. Please refer ~~feature/fullapp~~ branch for other rest apis like get account and create account (branch is currently outdated).


**API information:**

Method: PUT

Endpoint: /api/accounts/{source_account_id}/transfer/{destination_account_id}

Example Endpoint: /api/accounts/1/transfer/2

Request Body(JSON): {"cents": 500}


**Curl Request:**

`curl --location --request PUT 'http://localhost:8080/api/accounts/1/transfer/2' \
 --header 'Content-Type: application/json' \
 --data-raw '{"cents": "500"}'`

**Expected Response**
1. Success: {"data":{"id":1,"title":"Account 1","cents":500}}
2. Bad Request: When transfer amount is in string, negative or more then the available balance in source account.
3. Not Found: When source or destination account is not present in database (with id 3 or more)  

**Testing**

Unit and integration test report are also added in the repository(Only for review purpose) which can be found in the _intellij_idea_coverage_ directory. 
1. Class coverage 100% (14/ 14)
2. Method coverage 100% (65/ 65)
3. Line coverage 98.2% (217/ 221)

For keeping it simple, following are not used
1. Swagger for api docs
2. Cucumber for BDD
3. Docker 
4. DB Connection Pooling


**TEST CASES IN GITLAB CI IS FAILING BECAUSE H2 DATABASE IS NOT GETTING REFRESH IN MAVEN @BEFORE TEST FUNCTION, WILL CHECK LATER !!**